﻿using System.ComponentModel.DataAnnotations;

namespace latihan_netcore.Models
{
    public class ModelBrand
    {
        [Required]
        public string? Name { get; set; }
        [Required]
        public string? Address { get; set; }
        [Required]
        public string? Telephone { get; set; }
        [Required]
        public string? Email { get; set; }
        public DateTime Date { get; set; } = DateTime.Now;
    }
}
