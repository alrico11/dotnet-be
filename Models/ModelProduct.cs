﻿using System.ComponentModel.DataAnnotations;

namespace latihan_netcore.Models
{
    public class ModelProduct
    {
        [Required]
        public int? IDCompany { get; set; }
        [Required]
        public int? IDBrand { get; set; }
        [Required]
        public string? Name { get; set; }
        [Required]
        public string? Variant { get; set; }
        [Required]
        public int? Price { get; set; }
        [Required]
        public int? CreatedBy { get; set; }
        public DateTime Date { get; set; } = DateTime.Now;
    }
}
