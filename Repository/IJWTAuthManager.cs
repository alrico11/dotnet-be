﻿using Dapper;
using latihan_netcore.Models;

namespace latihan_netcore.Repository
{
    public interface IJWTAuthManager
    {
        Response<string> GenerateJWT(ModelUser user);
        Response<T> Execute_Command<T>(string query, DynamicParameters sp_params);
        Response<List<T>> getUserList<T>();
      
    }
}
