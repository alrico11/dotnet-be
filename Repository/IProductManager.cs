﻿using Dapper;
using latihan_netcore.Models;

namespace latihan_netcore.Repository
{
    public interface IProductManager
    {
        Response<T> Execute_Command<T>(string query, DynamicParameters sp_params);
        Response<List<T>> getProductList<T>();
    }
}
