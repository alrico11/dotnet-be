﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Dapper;
using latihan_netcore.Models;
using latihan_netcore.Repository;
using System.Data;
namespace latihan_netcore.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BrandController : Controller
    {
        private readonly IBrandManager _authentication;
        public BrandController(IBrandManager authentication)
        {
            _authentication = authentication;
        }
        [Microsoft.AspNetCore.Mvc.HttpPost("Create")]
        [Authorize(Roles = "Admin")]
        public IActionResult CreateBrand([System.Web.Http.FromBody] ModelBrand brand)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Parameter Is Missing");
            }
            DynamicParameters dp_param = new DynamicParameters();
            dp_param.Add("name", brand.Name);
            dp_param.Add("address", brand.Address);
            dp_param.Add("telephone", brand.Telephone);
            dp_param.Add("email", brand.Email);
            dp_param.Add("retVal", DbType.String, direction: ParameterDirection.Output);
            var result = _authentication.Execute_Command<ModelBrand>("sp_createBrand",dp_param);
            if(result.Code == 200)
            {
                return Ok(new { data = brand, message = "Success", code = 200 });
            }
            return BadRequest(result);
        }
        [Microsoft.AspNetCore.Mvc.HttpGet("Get")]
        [Authorize(Roles = "Admin")]
        public IActionResult getBrand()
        {
            var result = _authentication.getBrandList<ModelBrand>();

            return Ok(result);
        } 
        [Microsoft.AspNetCore.Mvc.HttpPut("Update")]
        public IActionResult updateBrand([System.Web.Http.FromBody] ModelBrand brand, string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Parameter invalid");
            }
            DynamicParameters dp_param = new DynamicParameters();
            dp_param.Add("id", id, DbType.String);
            dp_param.Add("name", brand.Name, DbType.String);
            dp_param.Add("address", brand.Address, DbType.String);
            dp_param.Add("telephone", brand.Telephone, DbType.String);
            dp_param.Add("email", brand.Email, DbType.String);
            dp_param.Add("retVal", DbType.String, direction: ParameterDirection.Output);
            var result = _authentication.Execute_Command<ModelUser>("sp_updateBrand", dp_param);
            if (result.Code == 200)
            {
                return Ok(new { data = brand, message = "Success", code = 200 });
            }

            return BadRequest(result);
        }
        [Microsoft.AspNetCore.Mvc.HttpDelete("Delete")]
        [Authorize(Roles = "Admin")]
        public IActionResult Delete(string id)
        {
            if (id == string.Empty)
            {
                return BadRequest("Parameter is missing");
            }

            DynamicParameters dp_param = new DynamicParameters();
            dp_param.Add("ID", id, DbType.String);
            dp_param.Add("retVal", DbType.String, direction: ParameterDirection.Output);

            var result = _authentication.Execute_Command<ModelBrand>("sp_deleteBrand", dp_param);

            if (result.Code == 200)
            {
                return Ok(result);
            }

            return NotFound(result);
        }
    }
}
