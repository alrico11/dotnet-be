﻿using Dapper;
using latihan_netcore.Models;
using latihan_netcore.Repository;
using Microsoft.AspNetCore.Mvc;
using System.Data;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;

namespace latihan_netcore.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : Controller
    {
        private readonly IProductManager _authentication;
        public ProductController(IProductManager authentication)
        {
            _authentication = authentication;
        }
        [Microsoft.AspNetCore.Mvc.HttpPost("Create")]
        [Authorize(Roles = "Admin")]
        public IActionResult Create([System.Web.Http.FromBody] ModelProduct product)
        {
            var userId = HttpContext.User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
            if (string.IsNullOrEmpty(userId))
            {
                return Unauthorized();
            }
            DynamicParameters dp_param = new DynamicParameters();
            dp_param.Add("idcompany", product.IDCompany);
            dp_param.Add("idbrand", product.IDBrand);
            dp_param.Add("name", product.Name);
            dp_param.Add("variant", product.Variant);
            dp_param.Add("price", product.Price);
            dp_param.Add("iduser", product.CreatedBy);
            dp_param.Add("retVal", DbType.String, direction: ParameterDirection.Output);

            var result = _authentication.Execute_Command<ModelProduct>("sp_createVariant", dp_param);
            if (result.Code == 200)
            {
                return Ok(new { data = product, message = "Success", code = 200 });
            }
            return BadRequest(result);
        }
        [Microsoft.AspNetCore.Mvc.HttpGet("Get")]
        [AllowAnonymous]
        [Authorize(Roles = "Admin")]
        public IActionResult getProduct()
        {
            var result = _authentication.getProductList<ModelProduct>();

            return Ok(result);
        }
        [Microsoft.AspNetCore.Mvc.HttpPut("Update")]
        [Authorize(Roles = "Admin")]
        public IActionResult updateProduct([System.Web.Http.FromBody] ModelProduct product, string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Parameter invalid");
            }
            DynamicParameters dp_param = new DynamicParameters();
            dp_param.Add("id", id, DbType.String);
            dp_param.Add("idcompany", product.IDCompany);
            dp_param.Add("idbrand", product.IDBrand);
            dp_param.Add("name", product.Name);
            dp_param.Add("variant", product.Variant);
            dp_param.Add("price", product.Price);
            dp_param.Add("iduser", product.CreatedBy);
            dp_param.Add("retVal", DbType.String, direction: ParameterDirection.Output);
            var result = _authentication.Execute_Command<ModelProduct>("sp_updateVariant", dp_param);
            if (result.Code == 200)
            {
                return Ok(new { data = product, message = "Success", code = 200 });
            }

            return BadRequest(result);
        }

    }


}
